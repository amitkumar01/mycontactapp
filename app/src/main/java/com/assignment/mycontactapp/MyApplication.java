package com.assignment.mycontactapp;

import android.app.Application;
/**
 * Created by Amit on 08/08/17.
 */

public class MyApplication extends Application {

    private static MyApplication self;

    public static synchronized MyApplication getInstance() {
        return self;
    }

    public static final MyApplication app() {
        return self;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        self = this;

    }

}
