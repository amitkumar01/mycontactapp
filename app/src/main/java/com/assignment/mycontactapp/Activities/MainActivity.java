package com.assignment.mycontactapp.Activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import com.assignment.mycontactapp.Adapters.MyContactRecyclerAdapter;
import com.assignment.mycontactapp.EntityClasses.ContactModel;
import com.assignment.mycontactapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity{

    private final int REQ_CODE_READ_CONTACT_PERMISSION = 105;
    private RecyclerView myContactRecyclerView;
    private MyContactRecyclerAdapter myContactRecyclerAdapter;
    private static ArrayList<ContactModel> arrayList = new ArrayList<>();
    private Parcelable recyclerViewState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myContactRecyclerView = (RecyclerView) findViewById(R.id.myContactRecyclerView);
        myContactRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        myContactRecyclerView.setHasFixedSize(true);
        recyclerViewState = myContactRecyclerView.getLayoutManager().onSaveInstanceState();
        myContactRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        myContactRecyclerAdapter = new MyContactRecyclerAdapter(MainActivity.this, arrayList);
        myContactRecyclerAdapter.setHasStableIds(true);

        myContactRecyclerView.setAdapter(myContactRecyclerAdapter);


        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                //doSomethingForEachUniquePhoneNumber(MainActivity.this);
                new LoadContacts().execute();
            } else {
                readContact();
            }
        } else {
            // Pre-Marshmallow
            new LoadContacts().execute();
        }

        EditText editText = findViewById(R.id.search_editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        editText.clearFocus();
    }

    private void filter(String text) {
        ArrayList<ContactModel> filteredList = new ArrayList<>();
        filteredList.clear();
        for (ContactModel item : arrayList) {
            if (item.getContactName().toLowerCase().contains(text.toLowerCase()) || item.getContactNumber().contains(text)) {
                filteredList.add(item);
            }
        }

        myContactRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        //mChildRecyclerView.addItemDecoration(new DividerItemDecoration(MyPatientListActivity.this, 0));
        myContactRecyclerView.setHasFixedSize(true);
        recyclerViewState = myContactRecyclerView.getLayoutManager().onSaveInstanceState();
        myContactRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        myContactRecyclerAdapter = new MyContactRecyclerAdapter(MainActivity.this, filteredList);
        myContactRecyclerAdapter.setHasStableIds(true);
        myContactRecyclerView.setAdapter(myContactRecyclerAdapter);
        //myContactRecyclerAdapter.filterList(filteredList);

    }


        private class LoadContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            arrayList = readContacts();// Get contacts array list from this
            // method
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(arrayList.size()>0) {
                myContactRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                //mChildRecyclerView.addItemDecoration(new DividerItemDecoration(MyPatientListActivity.this, 0));
                myContactRecyclerView.setHasFixedSize(true);
                recyclerViewState = myContactRecyclerView.getLayoutManager().onSaveInstanceState();
                myContactRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
                myContactRecyclerAdapter = new MyContactRecyclerAdapter(MainActivity.this, arrayList);
                myContactRecyclerAdapter.setHasStableIds(true);
                myContactRecyclerView.setAdapter(myContactRecyclerAdapter);
            }


        }
    }



    private ArrayList<ContactModel> readContacts() {
        ArrayList<ContactModel> contactList = new ArrayList<ContactModel>();

        Uri uri = ContactsContract.Contacts.CONTENT_URI; // Contact URI
        Cursor contactsCursor = getContentResolver().query(uri, null, null,
                null, ContactsContract.Contacts.DISPLAY_NAME + " ASC "); // Return
        if (contactsCursor.moveToFirst()) {
            do {
                long contctId = contactsCursor.getLong(contactsCursor
                        .getColumnIndex("_ID")); // Get contact ID
                Uri dataUri = ContactsContract.Data.CONTENT_URI; // URI to get
                // data of
                // contacts
                Cursor dataCursor = getContentResolver().query(dataUri, null,
                        ContactsContract.Data.CONTACT_ID + " = " + contctId,
                        null, null);// Retrun data cusror represntative to
                // contact ID

                // Strings to get all details
                String displayName = "";
                String nickName = "";
                String homePhone = "";
                String mobilePhone = "";
                String workPhone = "";
                String photoPath = "" + R.drawable.user; // Photo path
                byte[] photoByte = null;// Byte to get photo since it will come
                // in BLOB
                String homeEmail = "";
                String workEmail = "";
                String companyName = "";
                String title = "";

                // This strings stores all contact numbers, email and other
                // details like nick name, company etc.
                String contactNumbers = "";
                String contactEmailAddresses = "";
                String contactOtherDetails = "";

                // Now start the cusrsor
                if (dataCursor.moveToFirst()) {
                    displayName = dataCursor
                            .getString(dataCursor
                                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));// get
                    // the
                    // contact
                    // name
                    do {
                        if (dataCursor
                                .getString(
                                        dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE)) {
                            nickName = dataCursor.getString(dataCursor
                                    .getColumnIndex("data1")); // Get Nick Name
                            contactOtherDetails += "NickName : " + nickName
                                    + "n";// Add the nick name to string

                        }

                        if (dataCursor
                                .getString(
                                        dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {

                            // In this get All contact numbers like home,
                            // mobile, work, etc and add them to numbers string
                            switch (dataCursor.getInt(dataCursor
                                    .getColumnIndex("data2"))) {
                                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                    homePhone = dataCursor.getString(dataCursor
                                            .getColumnIndex("data1"));
                                    contactNumbers +=  homePhone
                                            + ",";
                                    break;

                                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                    workPhone = dataCursor.getString(dataCursor
                                            .getColumnIndex("data1"));
                                    contactNumbers +=  workPhone
                                            + ",";
                                    break;

                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                    mobilePhone = dataCursor.getString(dataCursor
                                            .getColumnIndex("data1"));
                                    contactNumbers += mobilePhone + ",";
                                    break;

                            }
                        }
                        if (dataCursor
                                .getString(
                                        dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {

                            // In this get all Emails like home, work etc and
                            // add them to email string
                            switch (dataCursor.getInt(dataCursor
                                    .getColumnIndex("data2"))) {
                                case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
                                    homeEmail = dataCursor.getString(dataCursor
                                            .getColumnIndex("data1"));
                                    contactEmailAddresses += "Home Email : "
                                            + homeEmail + "\n";
                                    break;
                                case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
                                    workEmail = dataCursor.getString(dataCursor
                                            .getColumnIndex("data1"));
                                    contactEmailAddresses += "Work Email : "
                                            + workEmail ;
                                    break;

                            }
                        }

                        if (dataCursor
                                .getString(
                                        dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)) {
                            companyName = dataCursor.getString(dataCursor
                                    .getColumnIndex("data1"));// get company
                            // name
                            contactOtherDetails += "Company Name : "
                                    + companyName + "n";
                            title = dataCursor.getString(dataCursor
                                    .getColumnIndex("data4"));// get Company
                            // title
                            contactOtherDetails += "Title : " + title + "n";

                        }

                        if (dataCursor.getString(
                                        dataCursor.getColumnIndex("mimetype"))
                                .equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {
                            String photo = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo.PHOTO_URI));
                            photoByte = dataCursor.getBlob(dataCursor
                                    .getColumnIndex("data15")); // get photo in
                            // byte
                            if (photoByte != null) {
                                photoPath = photo;// finally get the
                            }

                        }

                    } while (dataCursor.moveToNext()); // Now move to next
                    // cursor

                    contactList.add(new ContactModel(Long.toString(contctId),
                            displayName, contactNumbers, contactEmailAddresses,
                            photoPath, contactOtherDetails, companyName));// Finally add
                    // items to
                    // array list
                }

            } while (contactsCursor.moveToNext());
        }
        return contactList;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void readContact() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.READ_CONTACTS},
                    REQ_CODE_READ_CONTACT_PERMISSION);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQ_CODE_READ_CONTACT_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    new LoadContacts().execute();

                    // Permission Granted

                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                    closeNow();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }




    private void closeNow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }
}
