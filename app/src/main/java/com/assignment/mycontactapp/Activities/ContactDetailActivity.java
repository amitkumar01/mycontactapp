package com.assignment.mycontactapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.assignment.mycontactapp.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContactDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        collapsingToolbarLayout.setTitle("My Toolbar Title");
       // collapsingToolbarLayout.setContentScrimColor(Color.GREEN);

        ImageView contactImageView = (ImageView) findViewById(R.id.contactImageHeader);
        TextView emailTextView = (TextView) findViewById(R.id.email_tv);
        TextView idTextView = (TextView) findViewById(R.id.contactId_tv);
        TextView companyTextView = (TextView) findViewById(R.id.companyName_tv);

        Intent intent = getIntent();
        if(intent != null){
            String image = intent.getStringExtra("contactImage");
            String name = intent.getStringExtra("contactName");
            String phone = intent.getStringExtra("contactNumber");
            String email = intent.getStringExtra("contactEmail");
            String id = intent.getStringExtra("contactId");
            String companyName = intent.getStringExtra("contactCompanyName");
            Bitmap bp = null;
            if(image != null){
                try {
                    bp = MediaStore.Images.Media
                            .getBitmap(getContentResolver(),
                                    Uri.parse(image));

                    if(bp != null) {
                        contactImageView.setImageBitmap(bp);
                        //holder.image.setPadding(0, 0, 0, 0);
                    }

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                /*bp  = BitmapFactory.decodeFile(image);
                if( bp !=null) {
                    contactImageView.setImageBitmap(bp);
                }*/
            }
            if(name != null){
                collapsingToolbarLayout.setTitle(name);
            }

            if(!id.equals("")){
                idTextView.setText("Contact Id : "+id);
            }

            if(!companyName.equals("")){
                companyTextView.setText(companyName);
            }

            if(phone != null){
               // phoneTextView.setText(phone);
                String currentString = phone.replaceAll("\\s", "");
                currentString = currentString.replace("-","");
                String[] separated = currentString.split(",");
                separated = new HashSet<String>(Arrays.asList(separated)).toArray(new String[0]);
                for(int i = 0; i< separated.length; i++){
                    if(!separated[i].equals("")){
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(
                                Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.phone_number_dynamic_layout, null);
                        TextView phoneNumberTV = (TextView) view.findViewById(R.id.phoneNumber_tv);
                        if(separated[i].contains("n")){
                            separated[i] = separated[i].replaceAll("n", "");
                        }
                        phoneNumberTV.setText(separated[i]);
                        LinearLayout container = (LinearLayout) findViewById(R.id.flContainer);
                        container.addView(view);
                    }
                }

            }
            if(!email.equals("")){
                emailTextView.setText(email);
            } else {
                emailTextView.setHint("Email : ");
            }
        }

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }
}
