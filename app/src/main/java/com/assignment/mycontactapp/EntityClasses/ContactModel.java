package com.assignment.mycontactapp.EntityClasses;

public class ContactModel {

    private String contactId, contactName, contactNumber, contactEmail,
            contactPhoto, contactOtherDetails, companyName;

    public ContactModel(String contactId, String contactName,
                        String contactNumber, String contactEmail, String contactPhoto,
                        String contactOtherDetails, String companyName) {
        this.contactId = contactId;
        this.contactName = contactName;
        this.contactEmail = contactEmail;
        this.contactNumber = contactNumber;
        this.contactPhoto = contactPhoto;
        this.contactOtherDetails = contactOtherDetails;
        this.companyName = companyName;
    }

    public String getContactID() {
        return contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getContactPhoto() {
        return contactPhoto;
    }

    public String getContactOtherDetails() {
        return contactOtherDetails;
    }

    public String getCompanyName() {
        return companyName;
    }

}
