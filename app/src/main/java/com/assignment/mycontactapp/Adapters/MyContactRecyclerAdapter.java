package com.assignment.mycontactapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.mycontactapp.Activities.ContactDetailActivity;
import com.assignment.mycontactapp.EntityClasses.ContactModel;
import com.assignment.mycontactapp.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MyContactRecyclerAdapter extends RecyclerView.Adapter<MyContactRecyclerAdapter.RecyclerViewHolders> {

    Context context;
    ArrayList<ContactModel> myContactArrayList;
    int count = 1;

    public MyContactRecyclerAdapter(Context context,
                                    ArrayList<ContactModel> myContactArrayList) {
        this.context = context;
        this.myContactArrayList = myContactArrayList;
        setHasStableIds(true);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutView = infalInflater.inflate(R.layout.my_contact_list_item, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {

        Bitmap bp = null;
        final ContactModel myContact = myContactArrayList.get(position);
        holder.name.setText(myContact.getContactName());
        if (myContact.getContactPhoto() != null) {
            try {
                bp = MediaStore.Images.Media
                        .getBitmap(context.getContentResolver(),
                                Uri.parse(myContact.getContactPhoto()));

                if(bp != null) {
                    holder.image.setImageBitmap(bp);
                    holder.image.setPadding(0, 0, 0, 0);
                }

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            /*bp = BitmapFactory.decodeFile(myContact.getContactPhoto());
            if(bp != null) {
                holder.image.setImageBitmap(bp);
                holder.image.setPadding(0, 0, 0, 0);
            }*/
        }

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDetailActivity = new Intent(context, ContactDetailActivity.class);
                goToDetailActivity.putExtra("contactImage", myContact.getContactPhoto());
                goToDetailActivity.putExtra("contactName", myContact.getContactName());
                goToDetailActivity.putExtra("contactNumber", myContact.getContactNumber());
                goToDetailActivity.putExtra("contactEmail", myContact.getContactEmail());
                goToDetailActivity.putExtra("contactId", myContact.getContactID());
                goToDetailActivity.putExtra("contactCompanyName", myContact.getCompanyName());
                //goToDetailActivity.putExtra("contactEmailType", myContact.getEmailType());
                context.startActivity(goToDetailActivity);
            }
        });

        //holder.serialNo.setText(String.valueOf(count)+".");
        count++;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return myContactArrayList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        TextView serialNo;
        TextView name;
        ImageView image;

        public RecyclerViewHolders(View convertView) {
            super(convertView);
            //serialNo = (TextView) itemView.findViewById(R.id.serialNo_tv);
            name = (TextView) itemView.findViewById(R.id.name_tv);
            image = (ImageView) itemView.findViewById(R.id.displayImage);

        }
    }

    public void filterList(ArrayList<ContactModel> filteredList) {
        myContactArrayList = filteredList;
        //setHasStableIds(true);

        notifyDataSetChanged();

    }

}
